@include('staffs/staff.app')
   <div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Progress Report</h3>
	</div>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Task Name</th>
				<th>Status</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($progressReport as $progress_reports)

				<?php
				$putcolor=$progress_reports->status;
				if ($putcolor=="In progress") {?>
				 	<tr style="background-color: #96efe7">
					<th>{{$progress_reports->id}}</th>
					<th>{{$progress_reports->task_name}}</th>
					<td>{{$progress_reports->status}}</td>
					<td>{{date('M j, Y', strtotime($progress_reports->created_at))}}</td>
					<td><a href="{{route('progress_report.edit',$progress_reports->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text">
						 <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> 
					</a><a href="../storage/upload/{{$progress_reports->file}}" download="{{$progress_reports->file }}" class='ng-button'><i class="fa fa-download"></i>Download</a></td>
				</tr>
				
				<?php  } 

				 elseif ($putcolor=="Not started") {?>

				 	<tr style="background-color: #ed1b31">
					<th>{{$progress_reports->id}}</th>
					<th>{{$progress_reports->task_name}}</th>
					<td>{{$progress_reports->status}}</td>
					<td>{{date('M j, Y', strtotime($progress_reports->created_at))}}</td>
					<td><a href="{{route('progress_report.edit',$progress_reports->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a><a href="../storage/upload/{{$progress_reports->file}}" download="{{$progress_reports->file }}" class='ng-button'> <i class="fa fa-download"></i>Download</a></td>

				<?php }
				 elseif ($putcolor=="Complete") {?>
				 	<tr style="background-color: #c4d832">
					<th>{{$progress_reports->id}}</th>
					<th>{{$progress_reports->task_name}}</th>
					<td>{{$progress_reports->status}}</td>
					<td>{{date('M j, Y', strtotime($progress_reports->created_at))}}</td>
					<td><a href="../storage/upload/{{$progress_reports->file}}" download="{{$progress_reports->file }}" class='ng-button'> <i class="fa fa-download"></i>Download</a></td>
				<?php  }
				 else{
				 	echo "";
				 }
				?>
				
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>
<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Task </h3>
	</div>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Task</th>
				<th>Cline ID</th>
				<th>Create Date</th>
				<th>Attach file</th>
			</thead>
			<tbody>
				@foreach($task as $tasks)
				
				<?php 
				$old_time=strtotime($tasks->endtaskdate);
				$current_time=date('Y/m/d');
				$currents_time=strtotime($current_time);
				$different=$currents_time-$old_time;
				$dates=($different/(60*60*24));
				if ($dates>'2') {?>
					<tr style="background-color: #ed1b31">
								<?php
									
									 ?>

									<th>{{$tasks->id}}</th>
									<th>{{$tasks->file_name}}</th>
									<th>{{$tasks->email}}</th>
									<th>{{date($tasks->created_at)}}</th>

									<td><a href="../storage/upload/{{$tasks->file}}" download="{{$tasks->file }}">Download</a></td>
									
								</tr>
					
				<?php } else{ ?>
					<tr style="background-color: #c4d832">
									<th>{{$tasks->id}}</th>
									<th>{{$tasks->file_name}}</th>
									<th>{{$tasks->email}}</th>
									<th>{{{date('M j, Y', strtotime($tasks->created_at))}}}</th>
									<td><a href="../storage/upload/{{$tasks->file}}" download="{{$tasks->file }}" class='ng-button'> <i class="fa fa-download"></i>Downlaod</a></td>
									
								</tr>
				<?php }
				?>
								
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>

 @include('staffs/staff_layout.footer')
