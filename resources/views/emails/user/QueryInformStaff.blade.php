@component('mail::message')
# New Query Information

You have a New Query form cline please chack your account.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
