@component('mail::message')
# New Task Information

You have a New task form cline please chack your account.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
