@include('ausers/auser.app')
<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Query List</h3>
	</div>

	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Query Name</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($query as $querys)

				<?php
				$putcolor=$querys->answer;
				if (empty($putcolor)) {?>
				 	<tr style="background-color: #96efe7">
					<th>{{$querys->id}}</th>
					<th>{{$querys->question}}</th>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('query.edit',$querys->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a><a href="{{route('query.show',$querys->id)}}" style="margin: 10px;" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span> </a></td>
				</tr>
				
				<?php  } 

				 else  {?>

				 	<tr style="background-color: #c4d832">
					<th>{{$querys->id}}</th>
					<th>{{$querys->question}}</th>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('query.show',$querys->id)}}" style="margin: 10px;" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a></td>

				<?php } ?>
				
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>
<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Task List</h3>
	</div>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Task</th>
				<th>Cline ID</th>
				<th>Create Date</th>
			</thead>
			<tbody>
				@foreach($task as $tasks)
				
				<?php 
				$descigin=$tasks->status;
				if ($descigin=='completed') {?>
					<tr style="background-color: #c4d832">
								<?php
									
									 ?>

									<th>{{$tasks->id}}</th>
									<th>{{$tasks->file_name}}</th>
									<th>{{$tasks->email}}</th>
									<th>{{date($tasks->created_at)}}</th>
									<th><a href="" style="padding-right: 8px">Delete</a></th>
								</tr>
					
				<?php } else{ ?>
					<tr style="background-color: #96efe7">
									<th>{{$tasks->id}}</th>
									<th>{{$tasks->file_name}}</th>
									<th>{{$tasks->email}}</th>
									<th>{{{date('M j, Y', strtotime($tasks->created_at))}}}</th>
									<th> 
										 <a href="{{route('task.show',$tasks->id)}}" style="margin: 10px;"  class="btn btn-primary a-btn-slide-text">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        <a href="{{route('task.edit',$tasks->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a>
									</th>
								</tr>
				<?php }
				?>
								
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>

 @include('staffs/staff_layout.footer')
