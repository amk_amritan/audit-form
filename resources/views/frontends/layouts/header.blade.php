<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{asset('/')}}" style="color: #DF0101">Audit Form </a>
        <button class="navbar-toggler navbar-toggler-center" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">

                <div class="dropdown">
                <button class="dropbtn" style="color: #01DF3A">REGISTER</button>
                <div class="dropdown-content">
                  <a href="">Basic User</a>
                  <a href="">Primium User</a>
                </div>
                </div>

            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{asset('service')}}" style="color: #01DF3A">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{asset('about')}}" style="color: #01DF3A">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{asset('planeTable')}}" style="color: #01DF3A">Service Type</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{asset('contact')}}" style="color: #01DF3A">Contact</a>
            </li>
            <div class="dropdown">
                <button class="dropbtn" style="color: #01DF3A">LOGIN</button>
                <div class="dropdown-content">
                  <a href="{{asset('nuser')}}">Basic User</a>
                  <a href="{{asset('auser')}}">Primium User</a>
                </div>
                </div>
          </ul>
        </div>
      </div>
    </nav>