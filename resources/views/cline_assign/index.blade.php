@extends('cline_assign.base')

@section('action-content')

<div class="row">
    <div class="col-md-10">
        <h3>Cline Assign</h3>
    </div>
    <div class="col-md-2">
        <a href="{{ url('cline_assign/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Add</strong></span></a>
    </div>
    <hr>

</div>
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <th>Sn</th>
                <th>Staff Name</th>
                <th>Staff Email</th>
                <th>Cline Name</th>
                <th>Cline Email</th>
                <th>Action</th>
            </thead>
            <tbody>
                <!-- statrt loop -->
                @foreach($info as $infos)

                <tr>
                    <th>{{$infos->id}}</th>
                    <th>{{$infos->staff_name}}</th>
                    <td>{{$infos->staff_id}}</td>
                    <td>{{$infos->cline_name}}</td>
                    <td>{{$infos->cline_Id}}</td>
                    <td>

                        {!! Form::open(['route' => ['cline_assign.destroy', $infos->id], 'method' => 'delete']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}
                    </td>

                </tr>
                @endforeach
                <!--End loop -->
            </tbody>
            
        </table>
        
    </div>
</div>

@endsection