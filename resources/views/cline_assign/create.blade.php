@extends('cline_assign.base')

@section('action-content')

        <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'cline_assign.store']) !!}

            <div class="form-title-row">
                <h1>Assign Task</h1>
            </div>

             <div class="form-row">
                <label>
                    <span> {{form::label('staff_name','Staff Name')}} </span>
                    <select name="staff_name">
                        @foreach($Staff as $staffs)

                            <option value="{{$staffs->email}}">{{$staffs->name}}</option>

                        @endforeach
                        
                    </select>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> {{form::label('staff_id','Staff Name')}} </span>
                    <select name="staff_id">
                        @foreach($Staff as $staffs)

                            <option value="{{$staffs->id}}">{{$staffs->email}}</option>

                        @endforeach
                        
                    </select>
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> {{form::label('cline_name','Clint Name')}} </span>
                    <select name="cline_name">
                        @foreach($Auser as $Ausers)

                            <option value="{{$Ausers->email}}">{{$Ausers->name}}</option>

                        @endforeach
                        
                    </select>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span> {{form::label('cline_Id','Clint Name')}} </span>
                    <select name="cline_Id">
                        @foreach($Auser as $Ausers)

                            <option value="{{$Ausers->id}}">{{$Ausers->email}}</option>

                        @endforeach
                        
                    </select>
                </label>
            </div>


            <div class="form-row" style="padding-left: 35%">
                    {{Form::submit('Insert', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>

@endsection
