@extends('ausers.query.base')


@section('action-content')

        <div class="form-basic">
        	{!! Form::model($querys ,['route'=>['query.update',$querys->id], 'method' => 'PUT']) !!}

            <div class="form-title-row">
                <h1>Update Query</h1>
            </div>

            <div class="form-row">
                <label> 
                    <span>Question</span>
                   {{Form::textarea('question', null)}}
                </label>
            </div>  

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Query'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
