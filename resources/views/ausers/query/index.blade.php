@extends('ausers.query.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Query</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('ausers/query/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span> </a>
	</div>
	<hr>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Query</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($query as $querys)

				<tr>
					<th>{{$querys->id}}</th>
					<td>{{$querys->question}}</td>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('query.show',$querys->id)}}" style="margin: 10px;"  class="btn btn-primary a-btn-slide-text">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        <a href="{{route('query.edit',$querys->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a>
					{!! Form::open(['route' => ['query.destroy', $querys->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
			{{$query->links()}}
		</div>
	</div>
</div>


@endsection