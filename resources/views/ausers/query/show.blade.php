@extends('ausers.query.base')

@section('action-content')
        <div class="row">
    <div class="col-md-10">
    </div>
    <hr>

</div>
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tc>
                    <th>Sn</th>
                    <th>{{$querys->id}}</th>
                </tc>
            </thead>
            <tbody>
                <tc>
                    <td>Query Title </td>
                    <td>{{$querys->question}}</td>
                </tc>
            </tbody>
            <tbody>
                <tc>
                    <td>Answer </td>
                    <td>{{$querys->answer}}</td>
                </tc>
            </tbody>
            <tbody>
                <tc>
                    <td>Create Date </td>
                    <td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
                </tc>
            </tbody>

            <tbody>
                
                <td><a href="{{route('query.edit',$querys->id)}}" style="margin: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a></td>
            </tbody>

            </tbody>
            
        </table>
        
    </div>
</div>

@endsection