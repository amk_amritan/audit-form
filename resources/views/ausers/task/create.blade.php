@extends('ausers.query.base')

@section('action-content')

        <div class="form-basic">
        	{!! Form::open(array('route'=>'task.store' , 'files'=>true)) !!}

            <div class="form-title-row">
                <h1>Create Task</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Task Name</span>
                   {{Form::text('file_name')}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Browse File</span>
                     {{Form::file('file')}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Description</span>
                     {{Form::textarea('description')}}
                </label>
            </div>
            <h5>Your Staff Id is:-
                @foreach($staff_info as $staff_inf)
                    {{$staff_inf->staff_name}}
                    @endforeach</h5>

            <div class="form-row">
                <label>
                    <span>Enter Staff ID</span>

                     {{Form::text('Staff_Id')}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Task Complet Date</span>

                     {{Form::date('endtaskdate')}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Submitted'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
