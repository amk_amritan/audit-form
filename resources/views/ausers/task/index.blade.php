@extends('ausers.query.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Task</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('ausers/task/create')}}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>
<?php
?>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			
			<thead>
				<th>Sn</th>
				<th>Task</th>
				<th>Create Date</th>
				<th>Attach file</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $tasks)
				<tr>
					<th>{{$tasks->id}}</th>
					<td>{{$tasks->file_name}}</td>
					<td>{{date('M j, Y', strtotime($tasks->created_at))}}</td>
					<td><a href="../storage/upload/{{$tasks->file}}" download="{{$tasks->file }}">{{$tasks->file}}</a></td>
					<td>



        <a href="{{route('task.show',$tasks->id)}}" style="margin: 10px;"  class="btn btn-primary a-btn-slide-text">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        <a href="{{route('task.edit',$tasks->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a>


						{!! Form::open(['route' => ['task.destroy', $tasks->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
			{{$task->links()}}
		</div>
	</div>
</div>

@endsection