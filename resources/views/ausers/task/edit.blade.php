@extends('ausers.query.base')

@section('action-content')

        <div class="form-basic">
        	{!! Form::model($task ,['route'=>['task.update',$task->id], 'method' => 'PUT', 'files'=>true]) !!}

            <div class="form-title-row">
                <h1>Edit Task</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Task Name</span>
                   {{Form::text('file_name', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Browse File</span>
                     {{Form::file('file', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Description</span>
                     {{Form::textarea('description', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Task'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
