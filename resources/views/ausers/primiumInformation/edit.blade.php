@extends('ausers.query.base')

@section('action-content')

<div class="form-basic">
        	{!! Form::model($information ,['route'=>['primiumInformation.update',$information->id], 'method' => 'PUT', 'files'=>true]) !!}

            <div class="form-title-row">
                <h1>Edit Information</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Name</span>
                   {{Form::text('name', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Email</span>
                     {{Form::text('email', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Phone</span>
                     {{Form::number('phone', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Information'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection