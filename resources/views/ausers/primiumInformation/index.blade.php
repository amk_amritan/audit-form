@extends('ausers.query.base')

@section('action-content')

<div class="row">
    <div class="col-md-10">
    </div>
    <hr>

</div>
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tc>
                    <th>Sn</th>
                    <th>{{$information->id}}</th>
                </tc>
            </thead>
            <tbody>
                <tc>
                    <td>Company Name </td>
                    <td>{{$information->name}}</td>
                </tc>
            </tbody>
            <tbody>
                <tc>
                    <td>Email </td>
                    <td>{{$information->email}}</td>
                </tc>
            </tbody>

            <tbody>
                <tc>
                    <td>Contact Number </td>
                    <td>{{$information->phone}}</td>
                </tc>
            </tbody>
           

            <tbody>
                
                <td><a href="{{route('primiumInformation.edit',$information->id)}}" style="margin: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a></td>
            </tbody>

            </tbody>
            
        </table>
        
    </div>
</div>

@endsection