                    <div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{url('auser')}}" class="simple-text" style="color: green">
                    PRIMIUM USER
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{ url('ausers/query') }}">
                        <i class="ti-panel"></i>
                        <p>View Query</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('ausers/task')}}">
                        <i class="ti-text" style="color: red"></i>
                        <p style="color: red">View Task</p>
                    </a>
                </li>

                <li>
                    <a href="{{url('ausers/primiumInformation')}}">
                        <i class="glyphicon glyphicon-user" style="color: red"></i>
                        <p style="color: red">My Account</p>
                    </a>
                </li>

                 <li>
                    <a href="{{url('ausers/task')}}">
                        <i class="ti-text" style="color: red"></i>
                        <p style="color: red">Vat Report</p>
                    </a>
                </li>

                <li>
                    <a href="{{url('ausers/task')}}">
                        <i class="ti-text" style="color: red"></i>
                        <p style="color: red">Report</p>
                    </a>
                </li>
                 <li>
                    <a href="{{url('ausers/task')}}">
                        <i class="ti-text" style="color: red"></i>
                        <p style="color: red">Monthly Report</p>
                    </a>
                </li>

                 <li>
                    <a href="{{url('ausers/task')}}">
                        <i class="glyphicon glyphicon-cog" style="color: red"></i>
                        <p style="color: red">Support</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        
                    </ul>

                </div>
            </div>
        </nav>