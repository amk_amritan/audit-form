                    <div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{url('admin')}}" class="simple-text" style="color: green">
                      ADMIN LOGIN
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{ url('adminQuery') }}">
                        <i class="glyphicon glyphicon-th-large" style="color: red"></i>
                        <p>View Query</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('clineTask') }}">
                        <i class="glyphicon glyphicon-tasks" style="color: red"></i>
                        <p style="color: green">Cline Task</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('Service') }}">
                        <i class="glyphicon glyphicon-modal-window" style="color: red"></i>
                        <p style="color: green">Services</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('Information') }}">
                        <i class="ti-view-list-alt" style="color: red"></i>
                        <p style="color: green">Set Information</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('adminTask')}}">
                        <i class="glyphicon glyphicon-tasks" style="color: red"></i>
                        <p style="color: green">View Task</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('Team_member')}}">
                        <i class="glyphicon glyphicon-user" style="color: red"></i>
                        <p style="color: green">View Team Memaber</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('Normal_user')}}">
                        <i class="glyphicon glyphicon-user" style="color: red"></i>
                        <p style="color: green">Normal User</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('Advanced_user')}}">
                        <i class="glyphicon glyphicon-user" style="color: red"></i>
                        <p style="color: green">Primium User</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('Staff_user')}}">
                        <i class="glyphicon glyphicon-user" style="color: red"></i>
                        <p style="color: green">Staff</p>
                    </a>
                </li>
                <li>
                    <a href="{{url('cline_assign')}}">
                        <i class="ti-map" style="color: red"></i>
                        <p style="color: green">Cline Assign</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="color: green">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        
                    </ul>

                </div>
            </div>
        </nav>