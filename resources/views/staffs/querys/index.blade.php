@extends('staffs.querys.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Query</h3>
	</div>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Query</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($query as $querys)

				<tr>
					<th>{{$querys->id}}</th>
					<td>{{$querys->question}}</td>
					<td><a href="{{route('querys.show',$querys->id)}}" style="margin: 10px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span> </a><a href="{{route('querys.edit',$querys->id)}}" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span></a></td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div>
			
		</div>
		
	</div>
</div>
<div class="text-center">
			{{ $query->links() }}
		</div>	

@endsection