@extends('staffs.progress_report.base')


@section('action-content')

        <div class="form-basic">
        	{!! Form::model($progress ,['route'=>['progress_report.update',$progress->id], 'method' => 'PUT' , 'files'=>true])  !!}

            <div class="form-title-row">
                <h1>Update Project Progress</h1>
            </div>

            <div class="form-row">
                <label> 
                    <span>Attachmnet File</span>
                   {{Form::file('file', null)}}
                </label>
            </div>  
            <div class="form-row">
                <label> 
                    <span> Project Progress</span>
                   {{Form::select('status', [
                   'Completed'=>'Completed',
                  'In progress'=>'In progress',
                  'Not Start'=>'Not Start',]
                   )}}
                </label>
            </div>  

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Project Progress'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
