
@extends('staffs.progress_report.base')


@section('action-content')
<div class="content">
            <div class="container-fluid">
    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        
                        {!! Form::open(array('route'=>'progress_report.store' , 'files'=>true)) !!}

            <div class="form-title-row">
                <h1>Create Projct Sheet</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Task Name </span>
                   {{Form::text('task_name')}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Attechment </span>
                   {{Form::file('file')}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Progress in % </span>
                   {{Form::select('status', [
                   'Complete'=>'Complete',
                  'In progress'=>'In progress',
                  'Not started'=>'Not started',]
                   )}}
                </label>
            </div>


            <div class="form-row">
                    {{Form::submit('Create', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>
        
@endsection