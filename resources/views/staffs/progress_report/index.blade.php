@extends('staffs.progress_report.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Project sheet</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('staffs/progress_report/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span>  </a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Task Name</th>
				<th>Create By</th>
				<th>Status</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($progressReport as $progress_reports)

				<tr>
					<th>{{$progress_reports->id}}</th>
					<td>{{$progress_reports->task_name}}</td>
					<td>{{$progress_reports->create_by}}</td>
					<td>{{$progress_reports->status}}</td>
					<td>{{date('M j, Y', strtotime($progress_reports->created_at))}}</td>
					<td><a href="{{route('progress_report.edit',$progress_reports->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span></a>

						{!! Form::open(['route' => ['progress_report.destroy', $progress_reports->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</tbody>
			
		</table>
		
	</div>
	<div class="text-center">
			{{$progressReport->links()}}
		</div>
</div>

@endsection