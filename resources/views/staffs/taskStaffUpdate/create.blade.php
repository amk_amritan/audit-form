@extends('staffs.taskStaffUpdate.base')


@section('action-content')

        <div class="form-basic">
        	{!! Form::open(array('route'=>'task.store' , 'files'=>true)) !!}

            <div class="form-title-row">
                <h1>Create Task</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Task Name</span>
                   {{Form::text('file_name', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Browse File</span>
                     {{Form::file('file', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Description</span>
                     {{Form::textarea('description', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Create Query'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
