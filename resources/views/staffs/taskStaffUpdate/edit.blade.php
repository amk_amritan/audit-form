@extends('staffs.taskStaffUpdate.base')


@section('action-content')

        <div class="form-basic">
        	{!! Form::model($task ,['route'=>['taskStaffUpdate.update',$task->id], 'method' => 'PUT']) !!}

            <div class="form-row">
                <label> 
                    <span> Project Progress</span>
                   {{Form::select('status', [
                   'Completed'=>'Completed',
                  'In progress'=>'In progress',
                  'Not Start'=>'Not Start',]
                   )}}
                </label>

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Task'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
