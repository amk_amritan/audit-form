@extends('staffs.taskStaffUpdate.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Task</h3>
	</div>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Task</th>
				<th>Cline ID</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $tasks)
								<tr>
									<th>{{$tasks->id}}</th>
									<th>{{$tasks->file_name}}</th>
									<th>{{$tasks->email}}</th>
									<th>{{{date('M j, Y', strtotime($tasks->created_at))}}}</th>
									<td><a href="../storage/upload/{{$tasks->file}}" download="{{$tasks->file }}" style="padding-right: 8px;" class="ng-button"><i class="fa fa-download"></i> Download</a><a href="{{route('taskStaffUpdate.edit',$tasks->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span></a></th>
								</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
			{{ $task->links() }}
		</div>
	</div>
</div>
@endsection
			

				

				