<!-- Services -->
@extends('frontends.app')
@include('frontends.layouts.header')
@section('content')

<div class="row">
<div class="col" style="padding-top: 120px">
    <ul class="price-box">
        <li class="title">Basic Plane</li>
        <li class="emph"><strong>$500</strong>/Month</li>
        <li><strong>30</strong>Query Per Month</li>
        <li><strong>No</strong>  Technical Support</li>
        <li><strong>No</strong> File Transfer</li>
        <li class="emph"><a href="" class="button">Sign up</a></li>
    </ul>
    
</div>

<div class="col" style="padding-top: 120px">
    <ul class="price-box best">
        <li class="title titleTwo">Primium Plane</li>
        <li class="emph"><strong>$500</strong>/Month</li>
        <li><strong>Unlimited </strong>Query Per Month</li>
        <li><strong>Yes</strong>  Technical Support</li>
        <li><strong>Yes </strong>File Transfer</li>
        <li class="emph"><a href="" class="button">Sign up</a></li>
    </ul>
    
</div>
</div>
<hr>
@include('frontends.layouts.footer')
@endsection
