@extends('frontends.app')
@include('frontends.layouts.header')
@section('content')

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
        <!-- Modal1 -->
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Accounts</h4>
                    <img src="{{asset('asset/images/g10.jpg')}}" alt=" " class="img-responsive">
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                        dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
                        book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- //Modal1 -->
    <!--//Header-->
        <!-- about inner -->
<div class="about-bottom inner-padding">
    <div class="container">
        
                <h3 class="title-txt">About Us</h3>
                <div class="about-bott-right">
                     <h5>Who We Are</h5>
                     <p>Masagni dolores eoquie int Basmodi temporant, ut laboreas dolore magnam aliquam kuytase uaeraquis autem vel eum iure reprehend.Unicmquam eius, Basmodi temurer sehsMunim.Masagni dolores eoquie int Basmodi temporant, ut laboreas dolore magnam aliquam kuytase uaeraquis autem vel eum iure reprehend.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
</div>
<div class="about-agile inner-padding">
    <div class="container">
        <h3 class="heading-agileinfo white-w3ls">Masagni dolores eoquie int Basmodi temporant, ut laboreas dolore magnam aliquam kuytase</h3>
        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia .</p>
            
    </div>
</div>
<!-- //about inner -->
<!-- Team -->
<div class="team">
        <div class="container">
        <h3 class="title-txt">Our Team</h3>
            <div class="agile_team_grids">
                <div class="col-md-3 agile_team_grid">
                    <div class="view view-sixth">
                        <img src="{{asset('asset/images/te1.jpg')}}" alt=" " class="img-responsive">
                        <div class="mask">
                            
                            <div class="w3ls-social-icons">
                                <a class="facebook" href="#"><span class="fa fa-facebook"></span></a>
                                <a class="twitter" href="#"><span class="fa fa-twitter"></span></a>
                                <a class="pinterest" href="#"><span class="fa fa-pinterest-p"></span></a>
                            </div>
                        </div>
                    </div>
                    <h4>Daniel</h4>
                    <p>Tax Advisor</p>
                </div>
                <div class="col-md-3 agile_team_grid">
                    <div class="view view-sixth">
                        <img src="{{asset('asset/images/te2.jpg')}}" alt=" " class="img-responsive">
                        <div class="mask">
                            
                            <div class="w3ls-social-icons">
                                <a class="facebook" href="#"><span class="fa fa-facebook"></span></a>
                                <a class="twitter" href="#"><span class="fa fa-twitter"></span></a>
                                <a class="pinterest" href="#"><span class="fa fa-pinterest-p"></span></a>
                            </div>
                        </div>
                    </div>
                    <h4>Mary Winkler</h4>
                    <p>Accounting</p>
                </div>
                <div class="col-md-3 agile_team_grid">
                    <div class="view view-sixth">
                        <img src="{{asset('asset/images/te3.jpg')}}" alt=" " class="img-responsive">
                        <div class="mask">
                            
                            <div class="w3ls-social-icons">
                                <a class="facebook" href="#"><span class="fa fa-facebook"></span></a>
                                <a class="twitter" href="#"><span class="fa fa-twitter"></span></a>
                                <a class="pinterest" href="#"><span class="fa fa-pinterest-p"></span></a>
                            </div>
                        </div>
                    </div>
                    <h4>James Mac</h4>
                    <p>Manager</p>
                </div>
                <div class="col-md-3 agile_team_grid">
                    <div class="view view-sixth">
                        <img src="{{asset('asset/images/te4.jpg')}}" alt=" " class="img-responsive">
                        <div class="mask">
                        
                            <div class="w3ls-social-icons">
                                <a class="facebook" href="#"><span class="fa fa-facebook"></span></a>
                                <a class="twitter" href="#"><span class="fa fa-twitter"></span></a>
                                <a class="pinterest" href="#"><span class="fa fa-pinterest-p"></span></a>
                            </div>
                        </div>
                    </div>
                    <h4>Smith Carls</h4>
                    <p>Tax Advisor</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //Team -->

            
@endsection
    
