
@extends('nusers.queryes.base')


@section('action-content')
<div class="content">
            <div class="container-fluid">
    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->
        

        <div class="form-basic">
                        {!! Form::open(['route'=>'queryes.store']) !!}

            <div class="form-title-row">
                <h1>Create Query</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Enter Query </span>
                   {{Form::textarea('question', null)}}
                </label>
            </div>
            <h5>Your Staff Id is:-
                @foreach($staff_info as $staff_inf)
                    {{$staff_inf->staff_id}}
                    @endforeach</h5>

            <div class="form-row">
                <label>
                    <span>Enter Staff ID</span>

                     {{Form::text('Staff_Id', null)}}
                </label>
            </div>

            <div class="form-row">
                    {{Form::submit('Create Query', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>
        
@endsection