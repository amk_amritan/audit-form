@extends('nusers.queryes.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Query</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('nusers/queryes/create') }}" class="bth bth-lg bth-block bth-primary" style="font-size: 15px;" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Query</th>
				<th>Answer</th>
				<th>Cline ID</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($query as $querys)

				<tr>
					<th>{{$querys->id}}</th>
					<td>{{$querys->question}}</td>
					<td>{{$querys->answer}}</td>
					<th>{{$querys->email}}</th>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('queryes.show',$querys->id)}}" style="margin: 20px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
						{!! Form::open(['route' => ['queryes.destroy', $querys->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div>
			{{$query->links()}}
		</div>
	</div>
</div>

@endsection