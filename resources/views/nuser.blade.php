@include('nusers/nuser.app')
<div class="row">
	<div class="col-md-10">
		<h3 style="color: green">All Query List</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('nusers/queryes/create') }}" class="bth bth-lg bth-block bth-primary" style="font-size: 15px;" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span> </a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Query Name</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($query as $querys)

				<?php
				$putcolor=$querys->answer;
				if (empty($putcolor)) {?>
				 	<tr style="background-color: #96efe7">
					<th>{{$querys->id}}</th>
					<th>{{$querys->question}}</th>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('queryes.edit',$querys->id)}}" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span></a><a href="{{route('queryes.show',$querys->id)}}" style="margin: 10px;" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a></td>
				</tr>
				
				<?php  } 

				 else  {?>

				 	<tr style="background-color: #c4d832">
					<th>{{$querys->id}}</th>
					<th>{{$querys->question}}</th>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('queryes.show',$querys->id)}}" style="margin: 10px;" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a></td>

				<?php } ?>
				
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>
 @include('nusers/nuser_layout.footer')
