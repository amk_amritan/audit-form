@extends('frontends.app')
@include('frontends.layouts.header')
@section('content')

<!DOCTYPE html>
<html>
<head>
    <title>Audit Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Best Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

 <link rel="stylesheet" href="form/css/form-basic.css">
<link rel="stylesheet" href="form/css/demo.css">
<!--// css -->
<!-- font -->
</head>
<body>


    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'aregister.store']) !!}

            <div class="form-title-row">
                <h1>Register Hear....</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Name </span>
                   {{Form::text('name', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Email Address </span>
                   {{Form::text('email', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Password </span>
                   {{Form::password('password', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Contact Number </span>
                   {{Form::text('phone', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 15%">
                    {{Form::submit('Register', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>

</body>

</html>
@endsection
