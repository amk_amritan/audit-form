<!DOCTYPE html>
<html>
<head>
    <title>Audit Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Best Industry Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/lightbox.css">
<link rel="stylesheet" href="css/font-awesome.css" type="text/css" media="all" />

 <link rel="stylesheet" href="form/css/form-basic.css">
     <link rel="stylesheet" href="form/css/demo.css">
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>


            <!-- FlexSlider-JavaScript -->
    <script defer src="js/jquery.flexslider.js"></script>
    <script type="text/javascript">
        $(function(){
            SyntaxHighlighter.all();
                });
                $(window).load(function(){
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function(slider){
                        $('body').removeClass('loading');
                    }
            });
        });
    </script>
    <!-- //FlexSlider-JavaScript -->
</head>
</head>
<body>
    <div class="nav-links"> 
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href=""><h1>Audit Form</h1></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav link-effect">
                    <li class="active"><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="#about" class="scroll">About</a></li>
                    <li><a href="#services" class="scroll">Services</a></li>
                    <li><a href="#login">Login/Register</a></li>    
                </ul>
            </div>
        </div>
    </nav>
</div>
    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'nregister.store']) !!}

            <div class="form-title-row">
                <h1>Register Hear....</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Name </span>
                   {{Form::text('name', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Email Address </span>
                   {{Form::text('email', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Password </span>
                   {{Form::password('password', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Contact Number  </span>
                   {{Form::text('phone', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 15%">
                    {{Form::submit('Register', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>

</body>
<div class="footer">
    <div class="container">
        <p>&copy; 2017  All Rights Reserved | Design by <a href="http://creativewave.info/"> Creative Wave Information Technology </a></p>
    </div>
</div>
<script src="js/pie-chart.js" type="text/javascript"></script>
          <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#df4914',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#df4914',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#df4914',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-4').pieChart({
                barColor: '#df4914',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                rotate: 90,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });
        });

    </script>
        <!-- start-smooth-scrolling -->
            <script type="text/javascript" src="js/move-top.js"></script>
            <script type="text/javascript" src="js/easing.js"></script>
            <script type="text/javascript">
                jQuery(document).ready(function($) {
                    $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
            </script>
    <!-- //start-smoth-scrolling -->
        <!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            */
                                
            $().UItoTop({ easingType: 'easeOutQuart' });
                                
            });
    </script>
</html>
