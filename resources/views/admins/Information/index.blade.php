@extends('admins.Information.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>Company Information</h3>
	</div>
	@if(empty($task))
	<div class="col-md-2">
		<a href="{{ url('Information/create') }}" class="bth bth-lg bth-block bth-primary" style="font-size: 15px;" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span> </a>
	</div>
	<hr>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Company Name</th>
				<th>Company Address</th>
				<th>Mobile Number</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $infroamtion)

				<tr>
					<th>{{$infroamtion->id}}</th>
					<td>{{$infroamtion->company_name}}</td>
					<td>{{$infroamtion->comapny_address}}</td>
					<td>{{$infroamtion->mobile_number}}</td>
					<td>{{$infroamtion->phone}}</td>
					<td>{{$infroamtion->email}}</td>
					<td>
						{!! Form::open(['route' => ['Information.destroy', $infroamtion->id], 'method' => 'delete']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>
@else
	
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Company Name</th>
				<th>Company Address</th>
				<th>Mobile Number</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $infroamtion)

				<tr>
					<th>{{$infroamtion->id}}</th>
					<td>{{$infroamtion->company_name}}</td>
					<td>{{$infroamtion->comapny_address}}</td>
					<td>{{$infroamtion->mobile_number}}</td>
					<td>{{$infroamtion->phone}}</td>
					<td>{{$infroamtion->email}}</td>
					<td>
						{!! Form::open(['route' => ['Information.destroy', $infroamtion->id], 'method' => 'delete']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>
@endif
@endsection