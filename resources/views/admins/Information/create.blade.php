@extends('admins.Information.base')

@section('action-content')

    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'Information.store']) !!}

            <div class="form-title-row">
                <h1>Insert Company Information</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Company Name </span>
                   {{Form::text('company_name', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Company Address </span>
                   {{Form::text('company_address', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Mobile Number </span>
                   {{Form::number('mobile_number', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Phone  </span>
                   {{Form::number('phone', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Email  </span>
                   {{Form::text('email', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Facebook Url  </span>
                   {{Form::text('facebook_url', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Twitter Url  </span>
                   {{Form::text('twitter_url', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
                    {{Form::submit('Insert Information', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>
        @endsection