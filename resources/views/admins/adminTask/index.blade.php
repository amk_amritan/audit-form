@extends('admins.adminTask.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Task</h3>
	</div>

	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Task</th>
				<th>Cline ID</th>
				<th>Create Date</th>
				<th>Attach file</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $tasks)

				<tr>
					<th>{{$tasks->id}}</th>
					<td>{{$tasks->file_name}}</td>
					<td>{{$tasks->email}}</td>
					<td>{{date('M j, Y', strtotime($tasks->created_at))}}</td>
					<td><a href="{{asset('upload_file/' . $tasks->file)}}"></a></td>
					<td><a href="{{ route('adminTask.show',$tasks->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        	{!! Form::open(['route' => ['adminTask.destroy', $tasks->id], 'method' => 'delete']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}

    		</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
				 {{ $task->links() }}
			</div>
		
	</div>
</div>

@endsection