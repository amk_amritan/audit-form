@extends('admins.adminTask.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>Show Single Task</h3>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tc>
					<th>Sn</th>
					<th>{{$task->id}}</th>
				</tc>
			</thead>
			<tbody>
				<tc>
					<td>Task Name </td>
					<td>{{$task->file_name}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Cline ID </td>
					<td>{{$task->email}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Description </td>
					<td>{{$task->description}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Attach File </td>
					<td>{{$task->file}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Create Date </td>
					<td>{{date('M j, Y', strtotime($task->created_at))}}</td>
				</tc>
			</tbody>

			</tbody>
			
		</table>
		
	</div>
</div>

@endsection