@extends('admins.adminQuery.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Query</h3>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Query</th>
				<th>Cline ID</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $querys)

				<tr>
					<th>{{$querys->id}}</th>
					<td>{{$querys->question}}</td>
					<th>{{$querys->email}}</th>
					<td>{{date('M j, Y', strtotime($querys->created_at))}}</td>
					<td><a href="{{route('adminQuery.show', $querys->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>

						{!! Form::open(['route' => ['query.destroy', $querys->id], 'method' => 'delete']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
				 {{ $task->links() }}
			</div>
	</div>
</div>
@endsection