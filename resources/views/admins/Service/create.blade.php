@extends('admins.Service.base') 
@section('action-content')

    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'Service.store']) !!}

            <div class="form-title-row">
                <h1>Insert New Service</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Enter Service Title </span>
                   {{Form::text('service_title', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Enter Service description </span>
                   {{Form::textarea('service_description', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
                    {{Form::submit('Create Service', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>
@endsection