@extends('admins.Service.base') 

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>Show Single Task</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('task/create') }}" class="bth bth-lg bth-block bth-primary" style="font-size: 15px;" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tc>
					<th>Sn</th>
					<th>{{$task->id}}</th>
				</tc>
			</thead>
			<tbody>
				<tc>
					<td>Task Name </td>
					<td>{{$task->service_title}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Cline ID </td>
					<td>{{$task->service_description}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Create Date </td>
					<td>{{date('M j, Y', strtotime($task->created_at))}}</td>
				</tc>
			</tbody>

			<tbody>
				
				<td><a href="{{ route('Service.edit', $task->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Edit</strong></span></a>

  
    </td>
			</tbody>

			</tbody>
			
		</table>
		
	</div>
</div>

@endsection