@extends('admins.Service.base') 
@section('action-content')

        <div class="form-basic">
        	{!! Form::model($service ,['route'=>['Service.update',$service->id], 'method' => 'PUT']) !!}

            <div class="form-title-row">
                <h1>Edit Service</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Service Name</span>
                   {{Form::text('service_title', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Description</span>
                     {{Form::textarea('service_description', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Service'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
