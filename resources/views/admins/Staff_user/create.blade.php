@extends('admins.Staff_user.base')

@section('action-content')

    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'Staff_user.store']) !!}

            <div class="form-title-row">
                <h1>Create New Staff  Account</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Name </span>
                   {{Form::text('name', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Email Address </span>
                   {{Form::text('email', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Password </span>
                   {{Form::password('password', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Contact Number  </span>
                   {{Form::text('number', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
                    {{Form::submit('Insert Information', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>
        @endsection