@extends('admins.Staff_user.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Staff</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('Staff_user/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Staff Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Starting Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $staffs)

				<tr>
					<th>{{$staffs->id}}</th>
					<td>{{$staffs->name}}</td>
					<td>{{$staffs->email}}</td>
					<td>{{$staffs->phone}}</td>
					<td>{{date('M j, Y', strtotime($staffs->created_at))}}</td>
					<td>



                         <a href="{{route('Staff_user.show',$staffs->id)}}" style="margin: 10px;"  class="btn btn-primary a-btn-slide-text">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        <a href="{{route('Staff_user.edit', $staffs->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a>


						{!! Form::open(['route' => ['Staff_user.destroy', $staffs->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
				 {{ $task->links() }}
			</div>
		
	</div>
</div>

@endsection