@extends('admins.Staff_user.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
	</div>
	<div class="col-md-2">
		<a href="{{ url('Staff_user/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tc>
					<th>Sn</th>
					<th>{{$task->id}}</th>
				</tc>
			</thead>
			<tbody>
				<tc>
					<td>Staff Name </td>
					<td>{{$task->name}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Email </td>
					<td>{{$task->email}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Phone </td>
					<td>{{$task->phone}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Create Date </td>
					<td>{{date('M j, Y', strtotime($task->created_at))}}</td>
				</tc>
			</tbody>

			<tbody>
				
				<td><a href="{{route('Staff_user.edit', $task->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Edit</strong></span></a></td>
			</tbody>

			</tbody>
			
		</table>
		
	</div>
</div>

@endsection