@extends('admins.Normal_user.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Basic User</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('Normal_user/create') }}" class="bth bth-lg bth-block bth-primary" style="font-size: 15px;" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span> </a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Name</th>
				<th>Email</th>
				<th>Create Date</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $nusers)

				<tr>
					<th>{{$nusers->id}}</th>
					<td>{{$nusers->name}}</td>
					<td>{{$nusers->email}}</td>
					<td>{{date('M j, Y', strtotime($nusers->created_at))}}</td>
					<td>


                        <a href="{{route('Normal_user.show',$nusers->id)}}" style="margin: 10px;"  class="btn btn-primary a-btn-slide-text">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        <a href="{{route('Normal_user.edit', $nusers->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a>


						{!! Form::open(['route' => ['Normal_user.destroy', $nusers->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		<div class="text-center">
			{{$task->links()}}
		</div>
	</div>
</div>

@endsection