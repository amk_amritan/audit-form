@extends('admins.clineTask.base')

@section('action-content')

<div class="row">
    <div class="col-md-10">
        <h3>Cline List</h3>
    </div>
    <hr>

</div>
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <th>Sn</th>
                <th>Cline Name</th>
                <th>Staff Email</th>
                <th>Action</th>
            </thead>
            <tbody>
                <!-- statrt loop -->
                @foreach($Ausers as $infos)

                <tr>
                    <th>{{$infos->id}}</th>
                    <th>{{$infos->name}}</th>
                    <td>{{$infos->email}}</td>
                    <td><a href="{{route('clineTask.show',$infos->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
                    </td>

                </tr>
                @endforeach
                <!--End loop -->
            </tbody>
            
        </table>
        
    </div>
</div>

@endsection