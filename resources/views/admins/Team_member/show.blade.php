@extends('admins.Team_member.base')
@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>Show Detail Team Member</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('Team_member/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tc>
					<th>Sn</th>
					<th>{{$team->id}}</th>
				</tc>
			</thead>
			<tbody>
				<tc>
					<td> Name </td>
					<td>{{$team->name}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Address </td>
					<td>{{$team->address}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Mobile Number </td>
					<td>{{$team->mobile_number}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Roll </td>
					<td>{{$team->rool}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Email </td>
					<td>{{$team->email}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Facebook ID </td>
					<td>{{$team->facebook_url}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Twitter ID </td>
					<td>{{$team->twitter_url}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Create Date </td>
					<td>{{date('M j, Y', strtotime($team->created_at))}}</td>
				</tc>
			</tbody>

			<tbody>
				
				<td><a href="{{route('Team_member.edit',$team->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Edit</strong></span></a></td>
			</tbody>

			</tbody>
			
		</table>
		
	</div>
</div>

@endsection