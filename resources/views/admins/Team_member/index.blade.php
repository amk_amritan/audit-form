@extends('admins.Team_member.base')

@section('action-content')

<div class="row">
	<div class="col-md-10">
		<h3>All Team Memaber</h3>
	</div>
	<div class="col-md-2">
		<a href="{{ url('Team_member/create') }}" class="btn btn-primary a-btn-slide-text" style="font-size: 15px;"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span> </a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<th>Sn</th>
				<th>Name</th>
				<th>Address</th>
				<th>Mobile Number</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($task as $team)

				<tr>
					<th>{{$team->id}}</th>
					<td>{{$team->name}}</td>
					<td>{{$team->address}}</td>
					<td>{{$team->mobile_number}}</td>
					<td>


                        <a href="{{route('Team_member.show',$team->id)}}" style="margin: 10px;"  class="btn btn-primary a-btn-slide-text">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
        <span><strong>View</strong></span></a>
        <a href="{{route('Team_member.edit', $team->id)}}" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Update</strong></span> </a>


						{!! Form::open(['route' => ['Team_member.destroy', $team->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</td>

				</tr>
				@endforeach
			</tbody>
			
		</table>
		
	</div>
</div>

@endsection