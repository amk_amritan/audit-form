@extends('admins.Team_member.base')
@section('action-content')

    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <div class="form-basic">
                        {!! Form::open(['route'=>'Team_member.store']) !!}

            <div class="form-title-row">
                <h1>Create New Team Member</h1>
            </div>

            <div class="form-row">
                <label>
                    <span> Name </span>
                   {{Form::text('name', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Address </span>
                   {{Form::text('address', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Mobile Number </span>
                   {{Form::number('mobile_number', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Role  </span>
                   {{Form::text('role', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Email  </span>
                   {{Form::text('email', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Facebook Url  </span>
                   {{Form::text('facebook_url', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span> Twitter Url  </span>
                   {{Form::text('twitter_url', null)}}
                </label>
            </div>

            <div class="form-row" style="padding-left: 35%">
                    {{Form::submit('Insert Information', array('class'=> 'btn btn-success btn-lg btn-block'))}}
                
            </div>
            {!! Form::close() !!} 

        </div>

    </div>
                        </div>
                </div>
            </div>
        </div>
        @endsection