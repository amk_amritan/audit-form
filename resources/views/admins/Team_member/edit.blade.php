@extends('admins.Team_member.base')
@section('action-content')

        <div class="form-basic">
        	{!! Form::model($team ,['route'=>['Team_member.update',$team->id], 'method' => 'PUT']) !!}

            <div class="form-title-row">
                <h1>Edit Mode</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>Name</span>
                   {{Form::text('name', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Address</span>
                     {{Form::text('address', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Mobile Number</span>
                     {{Form::number('mobile_number', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Roll</span>
                   {{Form::text('roll', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Email</span>
                     {{Form::text('email', null)}}
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Facebook ID</span>
                     {{Form::text('facebook_url', null)}}
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>Twitter ID</span>
                     {{Form::text('twitter_url', null)}}
                </label>
            </div>
            <div class="form-row" style="padding-left: 35%">
            	{{Form::submit('Update Information'), array('class'=> 'btn btn-success btn-lg btn-block')}}
                
            </div>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
