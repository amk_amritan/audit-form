@extends('admins.Advanced_user.base')
@section('action-content')

<div class="row">
	<div class="col-md-10">
	</div>
	<div class="col-md-2">
		<a href="{{ url('Advacned_user/create') }}" class="bth bth-lg bth-block bth-primary" style="font-size: 15px;" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        <span><strong>Create</strong></span></a>
	</div>
	<hr>

</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tc>
					<th>Sn</th>
					<th>{{$auser->id}}</th>
				</tc>
			</thead>
			<tbody>
				<tc>
					<td>Staff Name </td>
					<td>{{$auser->name}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Email </td>
					<td>{{$auser->email}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Phone </td>
					<td>{{$auser->phone}}</td>
				</tc>
			</tbody>
			<tbody>
				<tc>
					<td>Create Date </td>
					<td>{{date('M j, Y', strtotime($auser->created_at))}}</td>
				</tc>
			</tbody>

			<tbody>
				
				<td><a href="{{route('Advanced_user.edit', $auser->id)}}" style="padding-right: 8px" class="btn btn-primary a-btn-slide-text"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
        <span><strong>Edit</strong></span></a></td>
			</tbody>

			</tbody>
			
		</table>
		
	</div>
</div>

@endsection