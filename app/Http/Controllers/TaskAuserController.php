<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Task;
use Image;
use App\cline_assign;
use Mail;
use App\Mail\NewUserWelcome;
use App\Mail\TaskInfromStaff;

class TaskAuserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email=session()->get('user_name');
        //$task = DB::select('select * from tasks where email = ?', [$email]);
        $task = DB::table('tasks')->where('email', $email)->paginate(10);

        return view('ausers/task.index')
        ->withTask($task);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $email=session()->get('user_name');
        $staff_info = DB::select('select * from cline_assigns where cline_name = ?', [$email]);
        return view('ausers.task.create')->with('staff_info',$staff_info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        // $this ->Validate($request , array(
        //     'file_name' =>'required| max:300',
        //     'description' =>'required| max:500',
        //     'file' =>'required| max:1000',
        //     'email'=>'required| max :300',
        // ));
        //Store database
        // $staff_id_find=DB::select('select staff_Id from cline_assigns where cline_Id = ?', [$email]);
        $email=session()->get('user_name');
        // $staff_Id = DB::table('cline_assigns')->select('staff_Id')->where('cline_Id', $email)->first();
        // return $staff_Id->staff_Id;
        $staff_Id = cline_assign::where('cline_Id',$email)->pluck('staff_Id');  
        
        $query = new Task;
        $query->file_name = $request->file_name;
        $query->email=session()->get('user_name');
        $query->description = $request->description;
        $query->Staff_Id = $request->Staff_Id;
        $query->endtaskdate = $request->endtaskdate;
        $query->file = $request->file; 
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $file_name=time().'.'. $file->getClientOriginalExtension();
            $request->file->storeAs('public/upload',$file_name);
            $query->file=$file_name;
        }
         Mail::to($request->Staff_Id)->send(New TaskInfromStaff());
        $query->save();
        //Redirect Other page
        // return redirect()->route('task.show',$query->id);
        return redirect()->route('task.index',$query->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task =Task::find($id);
        return view('ausers/task.show')->withTask($task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task =Task::find($id);
        return view('ausers/task.edit')->withTask($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
                'file_name' => 'required',
                'description'  => 'required',
                'file'=>'required'
            ));
        // Save the data to the database
        $task = Task::find($id);

        $task->file_name = $request->input('file_name');
        $task->description = $request->input('description');
        $oldFileName=$task->file;
        $task->file=$request->input('file');
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $file_name=time().'.'. $file->getClientOriginalExtension();
            $request->file->storeAs('public/upload',$file_name);
            $task->file=$file_name;
            // Delete old file 

            // if(Storage::delete($oldFileName->path)){
            //     $oldFileName->delete();
            //     return "delete";
            }

        $task->save();

        // redirect with flash data to posts.show
        return redirect()->route('task.index', $task->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                $post = Task::find($id);

        $post->delete();

        return redirect()->route('task.index');
    }
    public function email()
    {
       // Mail::to($query->Staff_Id = $request->Staff_Id)->send(New NewUserWelcome());
        //return view('/admin');
    }
}
