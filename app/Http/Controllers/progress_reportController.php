<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\progress_report;
use Storage;

class progress_reportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email=session()->get('user_name');
       //$progress_report = DB::select('select * from progress_reports where create_by = ?', [$email])->paginate(1);
       $progress_report = DB::table('progress_reports')->where('create_by', $email)->paginate(10);

        return view('staffs.progress_report.index')->withProgressReport($progress_report);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
        return view('staffs.progress_report.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
             'task_name' =>'required| max:300',
             'file'=>'required',
        ));
        //Store database
        $query = new progress_report;
        $query->task_name = $request->task_name;
        $query->create_by =session()->get('user_name');
        $query->file = $request->file; 
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $file_name=time().'.'. $file->getClientOriginalExtension();
            $request->file->storeAs('public/upload',$file_name);
            $query->file=$file_name;
        }
         $query->status = $request->status;
        $query->save();
        //Redirect Other page
        return redirect()->route('progress_report.index',$query->id);
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projectProgress =progress_report::find($id);
        return view('staffs.progress_report.edit')->withProgress($projectProgress);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $progress = progress_report::find($id);
        $oldFileName=$progress->file;
        $progress->file = $request->input('file');
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $file_name=time().'.'. $file->getClientOriginalExtension();
            $request->file->storeAs('public/upload',$file_name);
             
            $progress->file=$file_name;
            // Delete old file 
            storage::delete('app/public/$oldFileName');
        }

        $progress->status = $request->input('status');
        $progress->save();

        // redirect with flash data to posts.show
        return redirect()->route('progress_report.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = progress_report::find($id);

        $post->delete();

        return redirect()->route('progress_report.index');
    }
}
