<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nuser;

class NormaluserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nuser =Nuser::paginate(10);
        return view('admins.Normal_user.index')->withTask($nuser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.Normal_user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            // 'question' =>'required| max:300'
        ));
        $nusers =Nuser::paginate(10);
        //Store database
        $query = new Nuser;
        $query->name = $request->name;
        $query->email = $request->email;
        $query->password = $request->password;

        $query->save();
        //Redirect Other page
       return view('admins.Normal_user.index')->with('task', $nusers);
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nuser =Nuser::find($id);
        return view('admins.Normal_user.show')->withnormalmember($nuser);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nuser =Nuser::find($id);
        return view('admins.Normal_user.edit')->withnormalmember($nuser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, array(
                'name' => 'required',
                'email'  => 'required',
                'phone'=>'required'
            ));
        // Save the data to the database
         $nusers =Nuser::paginate(10);
        $nuser = nuser::find($id);

        $nuser->name = $request->input('name');
        $nuser->email = $request->input('email');
        $nuser->phone=$request->input('phone');

        $nuser->save();

        // redirect with flash data to posts.show
        return view('admins.Normal_user.index')->with('task', $nusers);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = nuser::find($id);

        $post->delete();

        return redirect()->route('admins.Normal_user.index');
    }
}
