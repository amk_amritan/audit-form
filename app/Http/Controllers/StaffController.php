<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use Illuminate\Support\Facades\DB;
use App\progress_report;
use App\Task;


class StaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:staff');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
                $email=session()->get('user_name');
        //$progressReport=DB::select('select * from progress_reports where create_by = ?', [$email]);
        $progressReport = DB::table('progress_reports')->where('create_by', $email)->paginate(5);

        //$task=DB::select('select * from tasks where Staff_Id = ?', [$email]);
        $task = DB::table('tasks')->where('Staff_Id', $email)->paginate(5);

        return view('staff')
        ->withprogressReport($progressReport)
        ->with('task',$task);
    }
}
