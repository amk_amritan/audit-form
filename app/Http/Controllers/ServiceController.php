<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service =Service::all();
        return view('admins.Service.index')->withTask($service);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.Service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            'service_title' =>'required| max:300',
            'service_description' =>'required| max:300',
        ));
        //Store database
        $query = new service;
        $query->service_title = $request->service_title;
        $query->service_description = $request->service_description;
        
        $query->save();
        //Redirect Other page
        return redirect()->route('admins.Service.show',$query->id);
        // return redirect()->route('service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service =Service::find($id);
        return view('admins.Service.show')->withTask($service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service =Service::find($id);
        return view('admins.Service.edit')->withService($service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
                'service_title' => 'required',
                'service_description'  => 'required',
            ));
        // Save the data to the database
        $service = Service::find($id);

        $service->service_title = $request->input('service_title');
        $service->service_description = $request->input('service_description');

        $service->save();

        // redirect with flash data to posts.show
        return redirect()->route('admins.Service.show', $service->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
