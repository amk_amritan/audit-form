<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team_Memeber;

class Team_memeberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team =Team_Memeber::all();
        return view('admins.Team_member.index')->withTask($team);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.Team_Member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form

        //Store database
        $query = new Team_memeber;
        $query->name = $request->name;
        $query->address = $request->address;
        $query->mobile_number = $request->mobile_number;
        $query->roll = $request->roll;
        $query->email = $request->email;
        $query->facebook_url = $request->facebook_url;
        $query->twitter_url = $request->twitter_url;

        
        $query->save();
        //Redirect Other page
        return redirect()->route('admins.Team_member.show',$query->id);
        // return redirect()->route('service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team =Team_Memeber::find($id);
        return view('admins.Team_member.show')->withTeam($team);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team =Team_Memeber::find($id);
        return view('admins.Team_member.edit')->withTeam($team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, array(
        //         'name' => 'required',
        //         'email'  => 'required',
        //         'phone'=>'required'
        //     ));
        // Save the data to the database
        $team = Team_Memeber::find($id);

        $team->name = $request->input('name');
        $team->address = $request->input('address');
        $team->mobile_number=$request->input('mobile_number');
         $team->roll = $request->input('roll');
        $team->email = $request->input('email');
        $team->facebook_url=$request->input('facebook_url');
        $team->twitter_url=$request->input('twitter_url');

        $team->save();

        // redirect with flash data to posts.show
        return redirect()->route('admins.Team_member.show', $team->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Team_Memeber::find($id);

        $post->delete();

        return redirect()->route('admins.Team_member.index');
    }
}
