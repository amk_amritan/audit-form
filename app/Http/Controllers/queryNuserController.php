<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Query;
use Mail;
use App\Mail\QueryInformStaff;

class queryNuserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email=session()->get('user_name');
        //$query  = DB::select('select * from queries where email = ?', [$email]);
         $query = DB::table('queries')->where('email', $email)->paginate(10);

        return view('nusers/queryes/index')->withquery($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $email=session()->get('user_name');
        $query  = DB::select('select * from cline_assigns where cline_Id = ?', [$email]);
        return view('nusers.queryes.create')->with('staff_info',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            'question' =>'required| max:300'
        ));
        //Store database
        $query = new Query;
        $query->question = $request->question;
        $query->email=session()->get('user_name');
        $query->Staff_Id = $request->Staff_Id;
        Mail::to($request->Staff_Id)->send(New QueryInformStaff());
        $query->save();
        //Redirect Other page
        return redirect()->route('queryes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $querys =Query::find($id);
        return view('nusers.queryes.show')->withQuerys($querys);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $querys =Query::find($id);
        return view('nusers/queryes.edit')->withQuerys($querys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $query = Query::find($id);

        $query->question = $request->input('question');

        $query->save();

        // redirect with flash data to posts.show
        return redirect()->route('queryes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Query::find($id);

        $post->delete();

        return redirect()->route('queryes.index');
    }
}
