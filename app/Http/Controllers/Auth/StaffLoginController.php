<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use App\progress_report;

class StaffLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:staff');
	}
    public function showLoginForm(){
    	return view('auth.staff-login');
    }
    public function login(Request $request){
    	//validate the form data
    	$this->validate($request,[
    		'email'=>'required|email',
    		'password'=>'required|min:6'
    	]);
    //Attem Login
    	if (Auth::guard('staff')->attempt(['email' => $request->email, 'password' => $request ->password], $request->remember)) {
    		// if success then redirect to their intended location
            Session()->put('user_name', $request->email);
            $progress_report = DB::select('select * from progress_reports where create_by = ?', [$request->email]);

        // return view('staffs.progress_report.index')->withProgressReport($progress_report);
            
            
    		return redirect()->intended(route('staff'));
    	}
    	//if unsuccess then back login form
    	return redirect()->back()->withInput($request->only('email','remember'));
    	
    }
}
