<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuserLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:auser,admin');
	}
    public function showLoginForm(){
    	return view('auth.auser-login');
    }
    public function login(Request $request){
    	//validate the form data
    	$this->validate($request,[
    		'email'=>'required|email',
    		'password'=>'required|min:6'
    	]);
    //Attem Login
    	if (Auth::guard('auser')->attempt(['email' => $request->email, 'password' => $request ->password,'status'=>'1'], $request->remember)) {
    		// if success then redirect to their intended location
            Session()->put('user_name', $request->email);
    		return redirect()->intended(route('auser'));
    	}
    	//if unsuccess then back login form
    	return redirect()->back()->withInput($request->only('email','remember'));
    	
    }
}
