<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Task;
use Image;

class taskStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email=session()->get('user_name');
        //$task=DB::select('select * from tasks where Staff_Id = ?', [$email]);
        $task = DB::table('tasks')->where('Staff_Id', $email)->paginate(1);
        return view('staffs.taskStaffUpdate.index')
        ->with('task',$task);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('staffs/task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        // $this ->Validate($request , array(
        //     'file_name' =>'required| max:300',
        //     'description' =>'required| max:300',
        //     'file' =>'required| max:300',
        //     'email'=>'required| max :300',
        // ));
        //Store database
        $query = new Task;
        $query->file_name = $request->file_name;
        $query->email=session()->get('user_name');
        $query->description = $request->description;
        $query->file = $request->file;  
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $filename=time().'.'. $file->getClientOriginalExtension();
            $location=public_path('upload_file/' . $filename);
            Image::make($file)->save($location);
            $query->file=$filename;
        }
        
        $query->save();
        //Redirect Other page
        // return redirect()->route('task.show',$query->id);
        return redirect()->route('staffs/taskStaffUpdate.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task =Task::find($id);
        return view('staffs.taskStaffUpdate.show')->withTask($task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task =Task::find($id);
        return view('staffs.taskStaffUpdate.edit')->withTask($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, array(
        //         'file_name' => 'required',
        //         'description'  => 'required',
        //         'file'=>'required'
        //     ));
        // Save the data to the database
        $task = Task::find($id);

        $task->status=$request->input('status');

        $task->save();

        // redirect with flash data to posts.show
        return redirect()->route('taskStaffUpdate.index', $task->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
