<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 
use App\Nuser;

class NuserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:nuser,admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
                $email=session()->get('user_name');
        //$query=DB::select('select * from queries where email = ?', [$email]);
        $query = DB::table('queries')->where('email', $email)->paginate(10);

        return view('nuser')->with('query',$query);
    }
}