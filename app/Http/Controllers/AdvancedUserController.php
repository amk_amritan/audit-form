<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Auser;

class AdvancedUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $advanced_user =Auser::paginate(10);
        return view('admins.Advanced_user.index')->withTask($advanced_user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.Advanced_user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            'password' =>'required| max:300'
        ));
        $advanced_user =Auser::paginate(10);
        //Store database
        $query = new Auser;
        $query->name = $request->name;
        $query->email = $request->email;
        $passwords=Hash::make($request->password);
        $query->password = $passwords;


        $query->save();
        //Redirect Other page
        return view('ausers.primiumInformation.index')->with('task', $advanced_user);
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auser =Auser::find($id);
        return view('admins.Advanced_user.show')->withauser($auser);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auser =Auser::find($id);
        return view('admins.Advanced_user.edit')->withauser($auser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
                'name' => 'required',
                'email'  => 'required',
                'phone'=>'required'
            ));
        // Save the data to the database
        $auser = Auser::find($id);
        $advanced_user =Auser::paginate(10);

        $auser->name = $request->input('name');
        $auser->email = $request->input('email');
        $auser->phone=$request->input('phone');

        $auser->save();

        // redirect with flash data to posts.show
        return view('admins.Advanced_user.index')->with('task', $advanced_user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Auser::find($id);

        $post->delete();

        return redirect()->route('admins.Advanced_user.index');
    }
}
