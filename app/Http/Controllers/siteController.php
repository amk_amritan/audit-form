<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class siteController extends Controller
{
    public function about(){
    	return view('site/about');
    }

    public function contact(){
    	return view('site/contact');
    }
 public function service(){
    	return view('site/service');
    }
    public function planeTable(){
    	return view('site/planeTable');
    }

}

