<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Image;
use DB;

class adminTasksController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task =DB::table('tasks')->simplePaginate(10);
        return view('admins.adminTask.index')->withTask($task);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminTask.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        // $this ->Validate($request , array(
        //     'file_name' =>'required| max:300',
        //     'description' =>'required| max:300',
        //     'file' =>'required| max:300',
        //     'email'=>'required| max :300',
        // ));
        //Store database
        $query = new Task;
        $query->file_name = $request->file_name;
        $query->email=session()->get('user_name');
        $query->description = $request->description;
        $query->file = $request->file;  
        if ($request->hasFile('file')) {
            $file=$request->file('file');
            $filename=time().'.'. $file->getClientOriginalExtension();
            $location=public_path('upload_file/' . $filename);
            Image::make($file)->save($location);
            $query->file=$filename;
        }
        
        $query->save();
        //Redirect Other page
        // return redirect()->route('task.show',$query->id);
        return redirect()->route('admin.adminTask.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task =Task::find($id);
        return view('admins.adminTask.show')->withTask($task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task =Task::find($id);
        return view('admins.adminTask.edit')->withTask($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
                'file_name' => 'required',
                'description'  => 'required',
                'file'=>'required'
            ));
        // Save the data to the database
        $task = Task::find($id);

        $task->file_name = $request->input('file_name');
        $task->description = $request->input('description');
        $task->file=$request->input('file');

        $task->save();

        // redirect with flash data to posts.show
        return redirect()->route('admins.adminTask.show', $task->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
