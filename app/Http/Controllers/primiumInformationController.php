<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auser;

class primiumInformationController extends Controller
{
	public function index(){

		$information = Auser::where('email', '=', session()->get('user_name'))->first();
    	return view('ausers.primiumInformation.index')
    	->with('information' , $information);
	
	}
	public function edit($id){

		$information = Auser::where('email', '=', session()->get('user_name'))->first();
		 $Ausers =Auser::find($id);
		 return view('ausers.primiumInformation.edit')->with('information' , $information);

	}

    public function update(Request $request){
    	$information = Auser::where('email', '=', session()->get('user_name'))->first();

    	 $query = new Auser;
        $query->name = $request->name;
        $query->email = $request->email;
        $query->phone = $request->phone;
        $query->save();
        //Redirect Other page
        return view('ausers.primiumInformation.index')->with('information' , $information);
        // return redirect()->route('home');
    }
}
