<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\cline_assign;
use App\Task;
use DB;
use Mail;
use App\Mail\NewUserWelcome;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $staffinfo=DB::table('staff')
        //         ->join('cline_assigns','cline_assigns.staff_id','=','staff.id')
        //         ->select('staff.name','cline_assigns.cline_name')
        //         ->get();
        $staff =Staff::paginate(5);
        $task=task::paginate(10);
        $cline=cline_assign::paginate(10);
        return view('admin')
        ->with('staffinfo',$staff)
        ->with('cline',$cline)
        ->with('task',$task);
    }
    public function email()
    {
        Mail::to(Auth::user()->email)->send(New NewUserWelcome());
        return view('/admin');
    }
}
