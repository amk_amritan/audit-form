<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auser;
use App\Task;
use DB;
use Mail;
use Auth;
use App\Mail\NewUserWelcome;
class ClineTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_time=date('Y/m/d');
        $currents_time=strtotime($current_time);
        $monthTime=$currents_time-86400;
        $time = date("m/d/Y h:i:s A T",$monthTime);
        $Ausers =Auser::all();
        return view('admins.clineTask.index')
        ->with('Ausers', $Ausers);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        $Ausers =Auser::find($id);
        $task = DB::table('tasks')->where('email', $Ausers->email)->get();
        //$task = Task::find($id);
       // return $task;
        //$task=Task::find($id);
        return view('admins.clineTask.show')
        ->with('Ausers', $Ausers)
        ->with('task',$task);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Ausers =Auser::find($id);
        Mail::to($Ausers->email)->send(New NewUserWelcome());
        return view('admins.clineTask.index');
    }
}
