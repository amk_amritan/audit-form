<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Information;

class informationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Information =Information::all();
        return view('admins.Information.index')->withTask($Information);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.Information.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            // 'question' =>'required| max:300'
        ));
        $Information =Information::all();
        //Store database
        $query = new Information;
        $query->company_name = $request->company_name;
        $query->company_address = $request->company_address;
        $query->mobile_number = $request->mobile_number;
        $query->phone = $request->phone;
        $query->email = $request->email;
        $query->facebook_url = $request->facebook_url;
        $query->twitter_url = $request->twitter_url;

        $query->save();
        //Redirect Other page
        return view('admins.Information.index')->withTask($Information);;
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
