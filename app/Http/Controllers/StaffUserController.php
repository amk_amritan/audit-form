<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;

use Illuminate\Support\Facades\Hash;

class StaffUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff_user =Staff::paginate(10);
        return view('admins.Staff_user.index')->withTask($staff_user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.Staff_user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            // 'question' =>'required| max:300'
        ));
        //Store database
        $query = new Staff;
        $query->name = $request->name;
        $query->email = $request->email;
        $passwords=Hash::make($request->password);
        $query->password = $passwords;

        $query->save();
        //Redirect Other page
        return redirect()->route('admins.Staff_user.show',$query->id);
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff =Staff::find($id);
        return view('admins.Staff_user.show')->withTask($staff);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $staff =Staff::find($id);
        return view('admins.Staff_user.edit')->withTask($staff);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
                'name' => 'required',
                'email'  => 'required',
                'phone'=>'required'
            ));
        // Save the data to the database
        $staff_user =Staff::paginate(10);
        $staff = Staff::find($id);

        $staff->name = $request->input('name');
        $staff->email = $request->input('email');
        $staff->phone=$request->input('phone');

        $staff->save();

        // redirect with flash data to posts.show
        return view('admins.Staff_user.index')->withTask($staff_user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Staff::find($id);

        $post->delete();

        return redirect()->route('admins.Staff_user.index');
    }
}
