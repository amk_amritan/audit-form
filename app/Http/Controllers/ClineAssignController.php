<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auser;
use App\Nuser;
use App\Staff;
use App\cline_assign;

class ClineAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Auser =Auser::all();
        $Nuser=Nuser::all();
        $Staff=Staff::all();
        $task_assign=cline_assign::all();

        return view('cline_assign.index')->withInfo($task_assign);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Auser =Auser::all();
        $Nuser=Nuser::all();
        $Staff=Staff::all();
        return view('cline_assign.create')
        ->with('Auser',$Auser)
        ->with('Staff',$Staff);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            // 'question' =>'required| max:300'
        ));
        //Store database
        $query = new cline_assign;
        $query->staff_name = $request->staff_name;
        $query->staff_id = $request->staff_id;
        $query->cline_name = $request->cline_name;
        $query->cline_Id = $request->cline_Id;
        $query->save();
        //Redirect Other page
        return redirect()->route('cline_assign.index',$query->id);
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = cline_assign::find($id);

        $post->delete();

        return redirect()->route('cline_assign.index');
    }
}
