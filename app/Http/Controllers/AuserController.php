<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  
use App\Nuser;

class AuserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:auser,admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(){
                $email=session()->get('user_name');
        //$query=DB::select('select * from queries where email = ?', [$email]);
        //$task=DB::select('select * from tasks where Staff_Id = ?', [$email]);
        $query = DB::table('queries')->where('email', $email)->paginate(10);
        $task = DB::table('tasks')->where('email', $email)->paginate(10);

        return view('auser')
        ->with('query',$query)
        ->with('task',$task);
    }
    
}
