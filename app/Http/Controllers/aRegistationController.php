<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Auser;
use Illuminate\Support\Str;
use Mail;
use App\Mail\verifyEmail;
use Session;


class aRegistationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('aregister.aregister');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate form
        $this ->Validate($request , array(
            'password' =>'required| max:300'
        ));
        //Flash Message
        Session::flash('status','Varify Link will Send Email Please check Email and Login');
        //Store database
        $query = new Auser;
        $query->name = $request->name;
        $query->email = $request->email;
        $passwords=Hash::make($request->password);
        $query->password = $passwords;
        $query->phone = $request->phone;
        $query->verifyTocken=Str::random(40);
        $query->save();

        Session()->put('user_name', $request->email);
        //Send mail to new register user before login
        $thisUsers=Auser::findOrFail($query->id);
        $this->sendEmail($thisUsers);
        //Redirect Other page
        return redirect()->route('auth.auser-login');
        //return view('auth.auser-login');
        // return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auser =Auser::find($id);
        return view('Advanced_user.show')->withauser($auser);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auser =Auser::find($id);
        return view('Advanced_user.edit')->withauser($auser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
                'name' => 'required',
                'email'  => 'required',
                'phone'=>'required'
            ));
        // Save the data to the database
        $auser = Auser::find($id);

        $auser->name = $request->input('name');
        $auser->email = $request->input('email');
        $auser->phone=$request->input('phone');

        $auser->save();

        // redirect with flash data to posts.show
        return redirect()->route('Advanced_user.show', $auser->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function sendEmail($thisUsers){
        Mail::to($thisUsers['email'])->send(New verifyEmail($thisUsers));
    }
    public function verifyEmailFirst(){
        return view('email.verifyEmailFirst');
    }
    public function sendEmailDone($email,$verifyToken){

        $query= Auser::where(['email'=>$email,'verifyTocken'=>$verifyToken])->first();
        if ($query) {
             Auser::where(['email'=>$email,'verifyTocken'=>$verifyToken])->update(['status'=>'1','verifyTocken'=>NULL]);
             return redirect()->route('auth.auser-login');
        }
        else{
            return 'User Not found';
        }

    }
}
