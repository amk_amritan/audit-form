<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/Login', function () {
    return view('Login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function(){
	Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/', 'AdminController@index')->name('admin');
});
Route::prefix('staff')->group(function(){
	Route::get('/login','Auth\StaffLoginController@showLoginForm')->name('staff.login');
	Route::post('/login','Auth\StaffLoginController@login')->name('staff.login.submit');
	Route::get('/', 'StaffController@index')->name('staff');

});
Route::prefix('nuser')->group(function(){
	Route::get('/login','Auth\NuserLoginController@showLoginForm')->name('nuser.login');
	Route::post('/login','Auth\NuserLoginController@login')->name('nuser.login.submit');
	Route::get('/', 'NuserController@index')->name('nuser');
	//reset password 
	
	Route::post('/password/email', 'Auth\NuserForgotPasswordController@sendResetLinkEmail')->name('nuser.password.email');
  	Route::get('/password/reset', 'Auth\NuserForgotPasswordController@showLinkRequestForm')->name('nuser.password.request');
  Route::post('/password/reset', 'Auth\NuserResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\NuserResetPasswordController@showResetForm')->name('nuser.password.reset');

});
Route::prefix('auser')->group(function(){
	Route::get('/login','Auth\AuserLoginController@showLoginForm')->name('auser.login');
	Route::post('/login','Auth\AuserLoginController@login')->name('auser.login.submit');
	Route::get('/', 'AuserController@index')->name('auser');
	//reset password 

	Route::post('/password/email', 'Auth\AuserForgotPasswordController@sendResetLinkEmail')->name('auser.password.email');
  	Route::get('/password/reset', 'Auth\AuserForgotPasswordController@showLinkRequestForm')->name('auser.password.request');
  Route::post('/password/reset', 'Auth\AuserResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\AuserResetPasswordController@showResetForm')->name('auser.password.reset');

});

Route::resource('query','queryController');
Route::resource('adminQuery','adminQueryController');
Route::resource('adminTask','adminTasksController');
Route::resource('task','taskController');

//Staff Rout File

Route::resource('staffs/querys','queryStaffController');
Route::resource('staffs/taskStaffUpdate','TaskStaffController');
Route::resource('staffs/progress_report','progress_reportController');

// Basic User File Control Rout
Route::resource('nusers/queryes','queryNuserController');

//Advanced User Routs
Route::resource('ausers/query','queryAuserController');
Route::resource('ausers/task','TaskAuserController');
Route::resource('ausers/primiumInformation','primiumInformationController');


Route::resource('Information','InformationController');
Route::resource('Service','ServiceController');
Route::resource('Team_member','Team_memeberController');
Route::resource('Normal_user','NormaluserController');
Route::resource('Advanced_user','AdvancedUserController');
Route::resource('Staff_user','StaffUserController');
Route::resource('clineTask','ClineTaskController');


// Cline Assign Route 

Route::resource('cline_assign','ClineAssignController');

//Advanced User Register
Route::resource('aregister','aRegistationController');

//Basic User Registation

Route::resource('nregister','nRegistationController');



Route::get('/email','clineTaskController@email')->name('email');
Route::get('/email','AdminController@email')->name('sendEmail');
Route::get('/email','TaskAuserController@email')->name('sendEmail');
Route::get('/verifyEmailFirst','aRegistationController@verifyEmailFirst')->name('verifyEmailFirst');
Route::get('verify/{email}/{verifyTocken}','aRegistationController@sendEmailDone')->name('sendEmailDone');

//Register Redirect page

Route::get('auth.auser-login','Auth\AuserLoginController@showLoginForm')->name('auth.auser-login');


//Frontend Route Start hear ----

Route::get('/', function () {
    return view('index');
});
Route::get('/about' , 'siteController@about');
Route::get('/contact','siteController@contact');
Route::get('/service','siteController@service');
Route::get('/planeTable','siteController@planeTable');


